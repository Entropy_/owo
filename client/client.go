package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
)

func encrypt(plaintext []byte, key []byte) ([]byte, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func decrypt(ciphertext []byte, key []byte) ([]byte, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}
func validate(signature string, file []byte) {
	const prefix = "/home/twotonne/"
	const secretKeyring = prefix + ".gnupg/secring.gpg"
	const publicKeyring = prefix + ".gnupg/pubring.gpg"
	fileBuf := new(bytes.Buffer)
	fileBuf.Write(file)
	buf := new(bytes.Buffer)

	buf.WriteString(signature)

	bufferedReader, err := os.Create("tests/outfile")
	if err != nil {
		fmt.Println("Error creating outfile")
	}
	bufferedReader.Write(file)
	bufferedReader.Close()
	bufferedReader, err = os.Open("tests/outfile")
	defer bufferedReader.Close()
	fmt.Println(string(file))
	message, err := decrypt(file, []byte("CRYPTOGRAPHYFUCK"))
	if err != nil {
		fmt.Println(err)
		fmt.Println("Error decrypting message")
	}
	fmt.Println(string(message))
}

func main() {
	fmt.Println("Press \033[38;2;255;0;0m1\033[0m to send a GET request to \033[48;2;235;20;20mlocalhost\033[0m")
	var input string
	fmt.Scan(&input)
	for {

		switch input {
		case "1":
			client := &http.Client{}
			request, err := http.NewRequest("GET", "http://127.0.0.1:8080/example", nil)
			if err != nil {
				fmt.Println(err)
			}
			request.Header.Add("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")

			request.Header.Add("Accept", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
			resp, err := client.Do(request)
			//defer resp.Body.Close()
			if err != nil {
				fmt.Println("Error sending request.")
				fmt.Println(err)
			}
			serverSignature := resp.Header.Get("Authorization")
			fmt.Println(serverSignature)
			if err != nil {
				fmt.Println("Authentication error!")
			}

			object, err := ioutil.ReadAll(resp.Body)
			resp.Body.Close()
			if err != nil {
				fmt.Println("Error reading reponse.")
				fmt.Println(err)
			}
			beachBall := object
			fmt.Println(beachBall)
			validate(serverSignature, beachBall)

			if err != nil {
				fmt.Println("\033[;38;2;255;0;0mError\033[0m crafting request")
				fmt.Println(err)
			}

			input = "0"
		case "exit":
			os.Exit(0)
		default:
			input = "0"
			fmt.Scan(&input)
		}
	}
}
