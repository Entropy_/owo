package main

import (
	"encoding/json"
	"fmt"
)

type Person string

const (
	PersonContext = "@context"
	PersonTypeS   = "type"
	PersonName    = "name"
)

func DefinePerson(grr map[Person]string) map[Person]string {
	//	app.context = append(app.context, "@context")
	grr[PersonContext] = "https://www.w3.org/ns/activitystreams#Person"
	//	app.typeS = append(app.typeS, "type")
	grr[PersonTypeS] = "Person"
	//	app.name = append(app.name, "name")
	grr[PersonName] = "SnowCrashNetwork"
	return grr
}

func EncodePerson() {
	//var app Application
	blob := make(map[Person]string)
	grr := DefinePerson(blob)

	jsonEncodedPerson, err := json.Marshal(grr)
	if err != nil {
		fmt.Println("Error marshaling json.")
	}
	fmt.Println(string(jsonEncodedPerson))
}
