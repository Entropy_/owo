package main

import (
	"fmt"
	"strings"
)

func AssembleCel(object Object) {
	cel := fmt.Sprint("\033[1;1H\033[0m")
	wor := ""
	word := ""
	words := ""
	if len(object.content.content) >= 69 {
		wor = object.content.content[:23]
		word = object.content.content[22:45]
		words = object.content.content[45:68]
	}
	if len(object.content.content) < 23 {
		wor = "                       "
		for i := 0; i < len(object.content.content)/2; i++ {
			word += " "
		}
		word += object.content.content
		for i := len(word); i < 23; i++ {
			word += " "
		}
		words = "                       "
	}
	X := "111111"
	Y := "234567"
	object.position.X = strings.Split(X, "")
	object.position.Y = strings.Split(Y, "")
	cel += fmt.Sprint("\033[", object.position.Y[0], ";", object.position.X[0], "H \033[48;2;10;255;20m                       \033[0m")
	cel += fmt.Sprint("\033[", object.position.Y[1], ";", object.position.X[1], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", wor, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[", object.position.Y[2], ";", object.position.X[2], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", word, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[", object.position.Y[3], ";", object.position.X[3], "H\033[48;2;10;255;20m \033[48;2;10;10;20m", words, "\033[48;2;10;255;20m \033[0m")
	cel += fmt.Sprint("\033[", object.position.Y[4], ";", object.position.X[4], "H \033[48;2;10;255;20m                       \033[0m")
	cel += fmt.Sprint("\033[", object.position.Y[5], ";", object.position.X[5], "H\033[0m")

	fmt.Println(cel)
}

func main() {
	//var obj Object
	//AssembleCel(obj)
	//time.Sleep(3 * time.Second)
	//for i := 0; i < 64; i++ {
	//	fmt.Println(" ")
	//}
	//obj.content = "GREETINGS"
	//AssembleCel(obj)
	//time.Sleep(3 * time.Second)
	//for i := 0; i < 64; i++ {
	//	fmt.Println(" ")
	//}
	//obj.content = "I WENT TO THE STORE TODAY AND I HAD A PIECE OF TOAST AND THEN YOU THREW AN OCOTPUS AT MY WINDOW"
	//AssembleCel(obj)
	//time.Sleep(3 * time.Second)
	//for i := 0; i < 64; i++ {
	//	fmt.Println(" ")
	//}
	EncodeApp()
	EncodeGroup()
	EncodeOrg()
	EncodePerson()
	EncodeService()
}
