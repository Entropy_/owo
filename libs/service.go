package main

import (
	"encoding/json"
	"fmt"
)

type Service string

const (
	ServiceContext = "@context"
	ServiceTypeS   = "type"
	ServiceName    = "name"
)

func DefineService(grr map[Service]string) map[Service]string {
	//	app.context = append(app.context, "@context")
	grr[ServiceContext] = "https://www.w3.org/ns/activitystreams#Service"
	//	app.typeS = append(app.typeS, "type")
	grr[ServiceTypeS] = "Service"
	//	app.name = append(app.name, "name")
	grr[ServiceName] = "SnowCrashNetwork"
	return grr
}

func EncodeService() {
	//var app Application
	blob := make(map[Service]string)
	grr := DefineService(blob)

	jsonEncodedService, err := json.Marshal(grr)
	if err != nil {
		fmt.Println("Error marshaling json.")
	}
	fmt.Println(string(jsonEncodedService))
}
