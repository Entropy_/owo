package main

import (
	"encoding/json"
	"fmt"
)

type Application string

const (
	AppContext = "@context"
	AppTypeS   = "type"
	AppName    = "name"
)

func DefineApp(app map[Application]string) map[Application]string {
	//	app.context = append(app.context, "@context")
	app[AppContext] = "https://www.w3.org/ns/activitystreams#Application"
	//	app.typeS = append(app.typeS, "type")
	app[AppTypeS] = "Application"
	//	app.name = append(app.name, "name")
	app[AppName] = "SnowCrashNetwork"
	return app
}

func EncodeApp() {
	//var app Application
	blob := make(map[Application]string)
	app := DefineApp(blob)

	jsonEncodedApp, err := json.Marshal(app)
	if err != nil {
		fmt.Println("Error marshaling json.")
	}
	fmt.Println(string(jsonEncodedApp))
}
