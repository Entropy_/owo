package main

type celPos struct {
	Y []string
	X []string
}
type AttributedTo struct {
	typeS string
	name  string
}
type Attachment struct {
	typeS   string
	content string
	url     string
}
type Audience struct {
	typeS string
	name  string
}
type Content struct {
	content   string
	summary   string
	typeS     string
	mediaType string
}
type Context struct {
	context string
	summary string
	typeS   string
	items   []string
}
type Name struct {
	context string
	typeS   string
	name    string
}
type EndTime struct {
	context   string
	typeS     string
	name      string
	startTime string
	endTime   string
}
type Generator struct {
	typeS string
	name  string
}
type Icon struct {
	typeS  string
	name   string
	url    string
	width  int
	height int
}
type InReplyTo struct {
	summary string
	typeS   string
	content string
}
type Location struct {
	name  string
	typeS string
	place string
}
type Replies struct {
	typeS      string
	totalItems int
	items      []Object
}
type Tag struct {
	typeS string
	id    string
	name  string
}
type Object struct {
	attachment   Attachment
	attributedTo AttributedTo
	audience     Audience
	content      Content
	context      Context
	name         Name
	endTime      EndTime
	generator    Generator
	icon         Icon
	inReplyTo    InReplyTo
	location     Location
	//There is no use for this right now
	preview string
	//
	published string
	replies   Replies
	startTime string
	summary   string
	tag       Tag
	updated   string
	url       string
	to        string
	bto       string
	cc        string
	bcc       string
	mediaType string
	duration  string
	position  celPos
}
