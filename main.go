package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	"golang.org/x/crypto/openpgp"
)

func encrypt(plaintext []byte, key []byte) ([]byte, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func decrypt(ciphertext []byte, key []byte) ([]byte, error) {
	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.New("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}

func signer(path string) []byte {
	const prefix = "/home/twotonne/"
	const secretKeyring = prefix + ".gnupg/secring.gpg"
	const publicKeyring = prefix + ".gnupg/pubring.gpg"
	secRingBuf, err := os.Open(secretKeyring)
	defer secRingBuf.Close()

	if err != nil {
		fmt.Println("Error grabbing public/private keyring, have you generated keys yet?")
	}
	entitiesPrivate, err := openpgp.ReadKeyRing(secRingBuf)
	fmt.Println(entitiesPrivate)

	buff, err := os.Create("tests/Buff")
	if err != nil {
		fmt.Println("Error creating buffer file.")
	}
	data, err := ioutil.ReadFile(path)
	ciphertext, err := encrypt(data, []byte("CRYPTOGRAPHYFUCK"))
	buff.Close()
	if err != nil {
		fmt.Println("Error reading signed text.")
		fmt.Println(err)
	}
	return ciphertext
}

func main() {

	fmt.Println("\033[48;2;0;255;0m\033[38;2;255;0;0mServer started\033[0m")

	fmt.Println("Signing test json.")
	signedContent := signer("tests/object.json")
	fmt.Println("Signing complete.")
	for {
		mux := http.NewServeMux()
		mux.HandleFunc("/example", func(w http.ResponseWriter, req *http.Request) {
			// Before any call to WriteHeader or Write, declare
			// the trailers you will set during the HTTP
			// response. These three headers are actually sent in
			// the trailer.
			if req.Header.Get("Accept") == "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"" {
				fmt.Println("Accept header is correct.")
				w.Header().Set("Content-Type", "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"")
				//w.Header().Set("Authentication", signature)
				w.WriteHeader(http.StatusOK)

				w.Write(signedContent)
			}
		})
		http.ListenAndServe(":8080", mux)
	}
}
